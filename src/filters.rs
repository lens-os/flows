// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::{byte, object, packet};
use alloc::string::ToString;
use alloc::vec::Vec;
use lens_system::Result;

/// An implementation of object::Send that uses Display to render human-readable descriptions.
pub struct DisplaySendFilter<'a> {
    pub flow: &'a dyn packet::Send,
}

impl<'a> DisplaySendFilter<'a> {
    /// Create a `DisplaySendFilter` that wraps the given `flow`.
    pub fn new(flow: &'a dyn packet::Send) -> DisplaySendFilter {
        DisplaySendFilter { flow }
    }
}

impl<'a, T: ToString> object::Send<T> for DisplaySendFilter<'a> {
    fn send(&self, object: T) -> Result<()> {
        let mut str = object.to_string();
        let str = unsafe { str.as_bytes_mut() };
        match self.flow.send(str) {
            Ok(()) => Ok(()),
            Err(err) => Err(err),
        }
    }
}

/// An implementation of packet::Send that writes one line for each packet to an underlying byte flow.
pub struct LineSendFilter<'a> {
    pub flow: &'a dyn byte::Write,
}

impl<'a> LineSendFilter<'a> {
    /// Create a `LineSendFilter` that wraps the given `flow`.
    pub fn new(flow: &'a dyn byte::Write) -> LineSendFilter {
        LineSendFilter { flow }
    }
}

impl<'a> packet::Send for LineSendFilter<'a> {
    fn send(&self, packet: &mut [u8]) -> Result<()> {
        let mut line: Vec<u8> = packet.to_vec();
        line.push(b'\n');
        match self.flow.write(&line[..]) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}

/// An implementation of packet::Send that frames packets with leading 64-bit lengths and writes them to an underlying byte flow.
pub struct FramedSendFilter<'a> {
    pub flow: &'a dyn byte::Write,
}

impl<'a> FramedSendFilter<'a> {
    /// Create a `LineSendFilter` that wraps the given `flow`.
    pub fn new(flow: &'a dyn byte::Write) -> FramedSendFilter {
        FramedSendFilter { flow }
    }
}

impl<'a> packet::Send for FramedSendFilter<'a> {
    fn send(&self, packet: &mut [u8]) -> Result<()> {
        let header = (packet.len() as u64).to_be_bytes();
        let mut frame = header.to_vec();
        frame.extend_from_slice(packet);
        match self.flow.write(&frame[..]) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}
