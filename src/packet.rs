// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.

use alloc::vec::Vec;
use lens_system::Result;

/// A packet flow through which packets can be received.
pub trait Receive {
    /// Receive a packet up to `max_size` bytes long from the packet flow.
    fn receive(&self, max_size: usize) -> Result<Vec<u8>>;
}

/// A packet flow through which packets can be sent.
pub trait Send {
    /// Send the `packet` to the packet flow.
    fn send(&self, packet: &mut [u8]) -> Result<()>;
}

/// A packet flow that can be sent or received, but silently fails on all operations.
pub struct Null {}

impl Receive for Null {
    fn receive(&self, max_size: usize) -> Result<Vec<u8>> {
        Ok(Vec::new())
    }
}

impl Send for Null {
    fn send(&self, packet: &mut [u8]) -> Result<()> {
        Ok(())
    }
}

/// A pre-created `Null`.
pub static NULL: Null = Null {};
